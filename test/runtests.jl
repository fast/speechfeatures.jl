
using SpeechFeatures
using Test
using Statistics

@testset "Window functions" begin
    N = 10
    T = Float32

    w = RectangularWindow(T, N)
    ref = ones(T, N)
    @test all(w .≈ ref)
    @test eltype(w) == T

    w = HannWindow(T, N)
    ref = 0.5 .* (1 .- cos.(2π .* Vector(0:N-1) ./ N ))
    @test all(w .≈ ref)
    @test eltype(w) == T

    w = HammingWindow(T, N)
    ref = (T(25/46)) .- (1 - T(25/46)) .* cos.(T(2π) .* Vector(0:N-1) ./ N )
    @test all(w .≈ ref)
    @test eltype(w) == T
end

@testset "Features" begin
    source = URLAudioSource("https://catalog.ldc.upenn.edu/desc/addenda/LDC93S1.wav")
    data, props = load(source)

    initial_framesize = Int(props.fs/40)
    nb_frames = round((length(data)-initial_framesize)/Int(props.fs/100))
    X, props = Frames()((data, props))
    @test size(X) == (initial_framesize, nb_frames)

    stft_framesize = floor(initial_framesize/2)+1
    X, props = STFT()((X, props))
    @test size(X) == (stft_framesize, nb_frames)

    X, props = FBANK()((X, props))
    @test size(X) == (26, nb_frames)

    X, props = MFCC()((X, props))
    nceps = 13 # number of cepstral values to keep
    @test size(X) == (nceps, nb_frames)

    X, props = AddDeltas()((X, props))
    @test size(X) == (nceps*3, nb_frames)
end

#=
@testset "SpecAugment" begin
    source = URLAudioSource("https://catalog.ldc.upenn.edu/desc/addenda/LDC93S1.wav")
    signal, fs = loadaudio(source)

    # Create fbank matrix
    fbank_config = Dict("numfilters" => 80)
    fbank_fce = FBANK(fs, fbank_config)
    fbank     = fbank_fce(signal[:, 1], fs)
    fbank_orig = copy(fbank)

    # Check time-warping

    # Test time warping only
    dct_conf = Dict(
        "apply_time_warp"  => true,
	"apply_freq_mask"  => false,
        "apply_time_mask"  => false,
        )
    SA_obj = SpecAugment(dct_conf)
    fbank_sa = SA_obj(fbank)
    @test size(fbank_sa) == size(fbank_orig)   # Size should be same
    @test fbank_sa       != fbank_orig         # fbank not

    # Freq
    dct_conf = Dict(
        "apply_time_warp"  => false,
	"apply_freq_mask"  => true,
        "apply_time_mask"  => false,
        )
    SA_obj = SpecAugment(dct_conf)
    fbank_sa = SA_obj(fbank)

    max_zeroed_freqs = SA_obj.params["freq_mask_width_range"][2] * SA_obj.params["num_freq_mask"]
    zeroed_freqs     = sum(mean(fbank_sa, dims=2) .== 0)
    max_zeroed_times = SA_obj.params["time_mask_width_range"][2] * SA_obj.params["num_time_mask"]
    zeroed_times     = sum(mean(fbank_sa, dims=1) .== 0)
    @test 0 < zeroed_freqs <= max_zeroed_freqs
    @test zeroed_times == 0
    @test fbank_sa    != fbank_orig

    # Time
    dct_conf = Dict(
        "apply_time_warp"  => false,
	"apply_freq_mask"  => false,
        "apply_time_mask"  => true,
        )
    SA_obj = SpecAugment(dct_conf)
    fbank_sa = SA_obj(fbank)

    max_zeroed_freqs = SA_obj.params["freq_mask_width_range"][2] * SA_obj.params["num_freq_mask"]
    zeroed_freqs     = sum(mean(fbank_sa, dims=2) .== 0)
    max_zeroed_times = SA_obj.params["time_mask_width_range"][2] * SA_obj.params["num_time_mask"]
    zeroed_times     = sum(mean(fbank_sa, dims=1) .== 0)
    @test zeroed_freqs == 0
    @test 0 < zeroed_times <= max_zeroed_times
end
=#
