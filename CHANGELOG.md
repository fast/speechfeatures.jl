# Releases


## 0.9.0
### Added
* Features summary
### Changed
* Better features chaining

## 0.8.0
### Added
* spectral augmentation

## 0.7.0
### Changed
* simplified the user interface to have easy to get default features
* added tests for the STFT, FBANK and MFCC features

## 0.6.0
# Features
* On-the-fly feature extraction (`FbankExtractor`, `MfccExtractor` and `StftExtractor`)

## 0.5.1
### Fixed
* audio source properly display in notebook

## 0.5.0
* high-resolution filterbanks
* audio source

## 0.4.0
* Simpler and faster implementation of the STFT. Other functionality are
  untested and probably broken.


## 0.3.2
* Avoid GC pressure by doing most of the operations in `stft` and
  `mfcc` in-place.

## 0.3.1
* Renamed `FilterBank` to `filterbank` to homogenize the user interface.
  The previous `FilterBank` function is still exported but is mark
  as deprecated.

## 0.3.0
* Simplfied the code and refactored the user interface.
* Added Pluto notebook examples.

## 0.2.0
* The output features are in matrix form insteaad of array of arrays.

## 0.1.0
* initial release
