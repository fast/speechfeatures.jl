# SPDX-License-Identifier: CECILL-C

#======================================================================
Framing
======================================================================#

struct FrameMatrix{T<:AbstractFloat,W<:AbstractVector{T}} <: AbstractArray{T,2}
    x::Vector{T}
    framesize::Int64
    stride::Int64
    window::W
    pad::Int
    dropedge::Bool
end

function get_nb_columns(f::FrameMatrix)
    if f.dropedge
        Int(ceil((length(f.x) - f.framesize) / f.stride))
    else
        Int(ceil(length(f.x) / f.stride))
    end
end

function Base.size(f::FrameMatrix)
    f.framesize, get_nb_columns(f)
end

function Base.getindex(f::FrameMatrix{T}, i::Integer, j::Integer) where T
    # index in the original signal
    n = (j - 1) * f.stride + i
    n ≤ length(f.x) ? f.window[i] * f.x[n] : T(0)
end

#======================================================================
Liftering
======================================================================#

function makelifter(N, L)
	t = Vector(1:N)
	1 .+ L/2 * sin.(π * t / L)
end

#======================================================================
Pre-emphasis
======================================================================#

function preemphasis!(x, k)
	prev = x[1]
	for i in 2:length(x)
        prev2 = x[i]
		x[i] = x[i] - k*prev
		prev = prev2
	end
end

#======================================================================
Delta features
======================================================================#

function delta(X::AbstractMatrix{T}, deltawin::Int = 2) where T
    D, N = size(X)
    Δ = zeros(T, D, N)
    norm = T(2 * sum(collect(1:deltawin).^2))
    for n in 1:N
        for θ in 1:deltawin
            Δ[:, n] += (θ * (X[:,min(N, n+θ)] - X[:,max(1,n-θ)])) / norm
        end
    end
    Δ
end

#======================================================================
Window functions
======================================================================#

struct CosineWindow{T} <: AbstractVector{T}
    size::Int
    a₀::T
    p::T
end

Base.size(w::CosineWindow) = (w.size,)
Base.getindex(w::CosineWindow, n) = (w.a₀ - (1 - w.a₀) * cos(2π * (n - 1) / w.size))^w.p

HannWindow(T, N) = CosineWindow{T}(N, 1/2, 1)
PoveyWindow(T, N) = CosineWindow{T}(N, 1/2, 0.85)
HammingWindow(T, N) = CosineWindow{T}(N, 25/46, 1)

struct RectangularWindow{T} <: AbstractVector{T}
    size::Int
end

RectangularWindow(T, N) = RectangularWindow{T}(N)
Base.size(w::RectangularWindow) = (w.size,)
Base.getindex(w::RectangularWindow, n) = 1

const windowTypes=Dict(
    "hann" => HannWindow,
    "hamming" => HammingWindow,
    "povey" => PoveyWindow,
    "rectangular" => RectangularWindow
)

#======================================================================
Filter bank
======================================================================#

mel2freq(mel::Real) = 700 * (exp(mel / 1127) - 1)
freq2mel(freq::Real) = 1127 * log(1 + (freq / 700))

# Create a set of triangular filters
function filterbank(n, binfreqs;
                    lofreq= 80,
                    hifreq = -400,
                    scale = freq2mel)

    # Convert the cut-off frequencies into mel
    nyquist = binfreqs[end]
    lomel = scale(lofreq)
    himel = scale(nyquist + hifreq)

    # Centers (in mel and freqs) of the filterbank
    melcenters = collect(range(lomel, himel, length = n + 2))

    # Allocate the matrix which will store the filters
    D = length(binfreqs) #div(fftlen, 2) + 1
    F = zeros(n, D)
    Δ = (himel - lomel) / (n + 1)

    # Construct the "triangle"
    for f in 1:n
        for d in 1:D
            m1 = lomel + (f-1)*Δ
            c = lomel + (f)*Δ
            m2 = lomel + (f+1) * Δ
            m = scale(binfreqs[d])

            F[f, d] =  if m < m1 || m >  m2
                0
            elseif m ≤ c
                1 + (m - c) / (c - m1)
            else
                1 + (m - c) / (c - m2)
            end
        end
    end
    F, melcenters
end

#======================================================================
Display
======================================================================#

function html_element(s::String, tag::String, options::String="")
    "<$tag $options>$s</$tag>"
end

function html_join(row::AbstractArray, tag::String)
    join(html_element.(row, tag))
end

function html_table(table::AbstractMatrix{String})
    html_element(
        html_join(
            html_join.(eachrow(table), "td")
        , "tr")
    , "table")
end

function html_table(table::AbstractMatrix{String}, header::AbstractVector{String})
    html_element(
        html_element(html_join(header, "th"), "tr")*
        html_join(
            html_join.(eachrow(table), "td")
        , "tr")
    , "table")
end

function html_table(table::AbstractMatrix{String}, header::AbstractVector{String}, title::String)
    html_element(
        html_element(html_element("$title", "th", "colspan=$(length(header))"), "tr")*
        html_element(html_join(header, "th"), "tr")*
        html_join(
            html_join.(eachrow(table), "td")
        , "tr")
    , "table")
end