using Interpolations
using Statistics

"""
y::Matrix = interpolated_spectral_resampling(x::Matrix,n_new::Int)

Use Gridded(Linear() interpolation to change number of columns in matrix x:
x(m,n) -> x(m,n_new)
"""
function interpolated_spectral_resampling(x::Matrix,n_new::Int)
  n=size(x,2)
  #println("Input size: $(n); Output size: $(n_new)")
  x_vec=[ x[:,i] for i in 1:n ]
  itp = interpolate((1:n,), x_vec, Gridded(Linear()))
  reduce(hcat, [ itp(i) for i in range(1, stop=n, length=n_new) ])
end

"""
y::Matrix = time_warp(x::Matrix; warp_window=5)

x(m,n) -> y(m,n)
Modify matrix in such a way that one part is downsamples and second upsampled over "n" axis.
"""
function time_warp(x::Matrix; warp_window=5::Int)
  n = size(x,2)  # time len

  if n-warp_window <= warp_window; return x; end
  center = rand(warp_window:(n-warp_window))
  warped = rand( (center-warp_window):(center + warp_window))

  left  = interpolated_spectral_resampling(x[:, 1:center], warped)
  right = interpolated_spectral_resampling(x[:,(center+1):n], (n - warped))

  hcat(left, right)
end

"""
y::Matrix = mask_along_axis(x::Matrix; axis=1::Int, mask_width_range=(0,30), num_mask=2::Int, replace_with_zero=true)


x(freq,time) -> y(freq,time)

Fill part of "x" by zeros or mean (based on replace_with_zero=true/false).
options:
 axis                    - 1 - freq, 2 - time
 mask_width_range=(0,30) - max range of mask width
 num_mask=2              - number of masks
"""
function mask_along_axis!(x::Matrix; axis=1::Int, mask_width_range=(0,30), num_mask=2::Int, replace_with_zero=true)
  @assert (axis==1 || axis==2) "Value of axis has to be 1 (freq) or 2 (time)"

  replace_with_zero ? value = 0.0 : value = mean(x)
  mask_length = rand( mask_width_range[1] : mask_width_range[2], num_mask)
  dim = size(x)[axis] # 1=freq; 2=time
  mask_pos    = rand(0 : findmax([1,dim - findmax(mask_length)[1]])[1], num_mask)

  aran=range(1,dim)
  mask = [ (aran .>= mask_pos[i]) .* (aran .< (mask_pos[i] + mask_length[i])) for i in 1:num_mask ]
  for i in 1:num_mask
    if axis==1; x[mask[i],:].=value; end
    if axis==2; x[:,mask[i]].=value; end
  end
  return x
end


"""
    struct SpecAugment <: SpeechFeatures.Features
        params
    end

Do spectral augmentation on input matrix of spectral features and return new augmented matrix with same size.
For more details read:
"SpecAugment: A Simple Data Augmentation Method for Automatic Speech Recognition", https://arxiv.org/abs/1904.08779

# --
Usage:
# Define spec.augmentation setting, these values are default:
sa_dict = Dict(
      "apply_time_warp" => true,
      "time_warp_window" => 5,
      "apply_freq_mask"  => true,
      "freq_mask_width_range" => (0, 15),
      "num_freq_mask"    => 2,
      "apply_time_mask"  => true,
      "time_mask_width_range" => (0, 70),
      "num_time_mask"    => 2
    )

SA_fce = SpecAugment(sa_dict)
output_spectra = SA_fce(input_spectra)
"""
struct SpecAugment <: SpeechFeatures.Features
  params

  function SpecAugment(config = Dict())
    defaults = Dict(
      "apply_time_warp" => true,
      "time_warp_window" => 5,
      "apply_freq_mask"  => true,
      "freq_mask_width_range" => (0, 15),
      "num_freq_mask"    => 2,
      "apply_time_mask"  => true,
      "time_mask_width_range" => (0, 70),
      "num_time_mask"    => 2
    )
    params = merge(defaults, config)
    new(params)
  end
end

"""
    Apply time warping and Spectral Augmentation over input matrix

    y::Matrix = (sa::SpecAugment)(feats:Matrix)
"""
function (sa::SpecAugment)(feats)
  if sa.params["apply_time_warp"]
    x = time_warp(feats, warp_window = sa.params["time_warp_window"])
  else
    x = copy(feats) # create new object for further processing
  end
  if sa.params["apply_freq_mask"]
    mask_along_axis!(x,axis=1, mask_width_range=sa.params["freq_mask_width_range"], num_mask=sa.params["num_freq_mask"])
  end
  if sa.params["apply_time_mask"]
    mask_along_axis!(x,axis=2, mask_width_range=sa.params["time_mask_width_range"], num_mask=sa.params["num_time_mask"])
  end
  return x
end
