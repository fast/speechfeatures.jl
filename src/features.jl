# SPDX-License-Identifier: CECILL-C

"""
    abstract type Features end

Abstract type of features extractors.
"""
abstract type Features end

struct SummarizableFeatures
    features::Features
end

function Base.summary(f::Features) 
    SummarizableFeatures(f)
end

#Overload show() for Features, only when called by summary() with html MIME type
function Base.show(io::IO, ::MIME"text/html", sumf::SummarizableFeatures)
    f = sumf.features
    properties = collect(propertynames(f))
	values = [getproperty(f, p) for p in properties]
    # matrix of String describing each property, its value and type
	description = Array{String}(undef, length(properties), 3)
	description[:,1] = string.(properties)
	description[:,2] = string.(values)
	description[:,3] = string.(typeof.(values))
    # get an HTML table
    name = string(typeof(f))
    header = ["field", "value", "type"]
    table = html_table(description, header, name)
    print(io, table)
end

"""
FeaturesProperties
Common properties to features
`fs` is the frequency sampling of the input to the feature extractor
`scale` is a Vector giving the feature resolution, in its given `unit`
"""
struct FeaturesProperties{T}
    fs_init::Int
    fs::Int
    scale::AbstractVector
    unit::String
end

const DataProps{T} = Tuple{AbstractArray, FeaturesProperties{T}}

struct SummarizableProperties
    props::FeaturesProperties
    scale_begin::Float64
    scale_end::Float64
    scale_elements::Int
end

function SummarizableProperties(props::FeaturesProperties)
    scale_begin = round(props.scale[begin], digits=2)
    scale_end = round(props.scale[end], digits=2)
    scale_elements = length(props.scale)
    SummarizableProperties(props, scale_begin, scale_end, scale_elements)
end


function Base.summary(props::FeaturesProperties)
    SummarizableProperties(props)
end

#Overload show() for FeaturesProperties, only when called by summary()
function Base.show(io::IO, sumprops::SummarizableProperties)
    props = sumprops.props
    println(io, "fs\t$(props.fs)\tHz")
    print(io, "scale   \t[$(sumprops.scale_begin), …, $(sumprops.scale_end)] $(sumprops.scale_elements) elements\t$(props.unit)")
end

#Overload show() for FeaturesProperties, only when called by summary() with html MIME type
function Base.show(io::IO, ::MIME"text/html", sumprops::SummarizableProperties)
    props = sumprops.props
    scalestr="[$(sumprops.scale_begin), …, $(sumprops.scale_end)] $(sumprops.scale_elements) elements"
    table = html_table(["fs" "$(props.fs)" "Hz"
                        "scale" "$scalestr" "$(props.unit)"
                       ])
    print(io, table)
end


# Rewrite load functions to add the option to return a FeaturesProperties object (enabled by default) 
function load_with_props(audio, fs)
    l = size(audio, 1)
    props = FeaturesProperties{AbstractAudioSource}(fs, fs, 0:l/fs, "s" )
    audio, props
end

function load(s::AbstractAudioSource; subrange=:, ch=1, props=true)
    audio, fs = AudioSources.load(s, subrange, ch)
    props ? load_with_props(audio, fs) : (audio, fs)
end

function load(s::Int; ch=1, props=true)
    audio, fs = AudioSources.load(s, ch)
    props ? load_with_props(audio, fs) : (audio, fs)
end

"""
    Frames <: Features
Segment resulting from framing the signals. This representation is usually used
to extract the short-term Fourier transform.
"""
struct Frames <: Features
    frameduration::Float64 # s
    stepduration::Float64 # s
    dithering::Float64
    preemph::Float64
    removedc::Bool
    windowname::String
    padding::Int
    dropedge::Bool

end

function Frames(; frameduration=0.025, stepduration=0.01, dithering=0.0, preemph=0.97, removedc=true, windowname="hann", padding=0, dropedge=true)
    Frames(frameduration, stepduration, dithering, preemph, removedc, windowname, padding, dropedge)
end

function (f::Frames)((x, props)::DataProps{AbstractAudioSource})
    # Properties
    framesize = get_framesize(f, props.fs)
    framestep = get_framestep(f, props.fs)

    haskey(windowTypes, f.windowname) || throw(ArgumentError("unkown window type $(f.windowname)"))
    # Copy the window, it allocates a buffer but save many computations.
    window = copy(windowTypes[f.windowname](Float64, framesize))

    scale=collect(0:1/props.fs:(framesize-1)/props.fs)
    newprops = FeaturesProperties{Frames}(props.fs_init, Int(props.fs/framestep), scale, "s")

    # Processing
    if f.removedc
        dc = sum(x) / length(x)
        x = map(a -> a - dc, x)
    end
    iszero(f.preemph) || preemphasis!(x, f.preemph)
    iszero(f.dithering) || map!(a -> a + f.dithering * randn(), x, x)
    append!(x, zeros(f.padding))
    result = FrameMatrix(x, framesize, framestep, window, f.padding, f.dropedge)
    (result, newprops)
end

get_framesize(f::Frames, fs::Int) = Int(ceil(f.frameduration*fs)) + f.padding
get_framestep(f::Frames, fs::Int) = Int(ceil(f.stepduration*fs))


"""
    STFT <: Features
Short-Term Fourier Transform.
"""
struct STFT <: Features
end

function (f::STFT)((X, props)::DataProps{Frames})
    # Properties
    newscale = rfftfreq(length(props.scale), props.fs_init)
    newprops = FeaturesProperties{STFT}(props.fs_init, props.fs, newscale, "Hz" )
    # Processing
    P = plan_rfft(X, 1)
    result = P * X
    (result, newprops)
end


"""
    FBANK <: Features
Mel spectrum.
"""
struct FBANK <: Features
    numfilters::Int
    lofreq::Int
    hifreq::Int
end

FBANK(; numfilters=26, lofreq=80, hifreq=-400) = FBANK(numfilters, lofreq, hifreq)


function (f::FBANK)((X, props)::DataProps{STFT})
    # Properties
    binfreqs = props.scale
    filters, melcenters = filterbank(f.numfilters, binfreqs, lofreq=f.lofreq, hifreq=f.hifreq)
    newprops = FeaturesProperties{FBANK}(props.fs_init, props.fs, melcenters, "mel")
    # Processing
    result = log.(filters * abs.(X))
    (result, newprops)
end


"""
    MFCC <: Features
Mel Frequency Cepstral Coefficients.
"""
struct MFCC <: Features
    nceps::Int
    liftering::Int
end

MFCC(; nceps=13, liftering=22) = MFCC(nceps, liftering)

function (f::MFCC)((X, props)::Union{DataProps{FBANK}, DataProps{STFT}} )
    # Properties
    newscale = 0:f.nceps-1
    newprops = FeaturesProperties{MFCC}(props.fs_init, props.fs, newscale, "quefrency")
    # Processing
    melcepstrum = dct(X, 1)[1:f.nceps,:]
    if f.liftering > 0
        lifter = makelifter(size(melcepstrum, 1), f.liftering)
        foreach(x -> x .*= lifter, eachcol(melcepstrum))
    end
    (melcepstrum, newprops)
end

"""
Autocorrelation
"""
struct Autocorr <: Features
end

function (f::Autocorr)((X, props)::DataProps{Frames})
    # Properties
    newprops = FeaturesProperties{Autocorr}(props.fs_init, props.fs, props.scale, props.unit) #"s"
    # Processing
    # Fourier transform
    FX = rfft(X)
    # inverse Fourier transform
    result = irfft(abs.(FX).^2, size(X,1))
    (result, newprops)
end


"""
Add the derivatives to the features.
"""
struct AddDeltas  <: Features
    order::Int
    winlen::Int
end

AddDeltas(; order=2, winlen=2) = AddDeltas(order,winlen)

function (f::AddDeltas)((X, props)::DataProps{<:Features})
    X_and_deltas = [X]
    for _ in 1:f.order
        push!(X_and_deltas, delta(X_and_deltas[end], f.winlen))
    end
    vcat(X_and_deltas...), props
end


unfoldchain(c::ComposedFunction{<:Any,<:Features}, v::Vector=[]) = unfoldchain(c.outer, vcat(v, c.inner))

unfoldchain(c::Features, v::Vector) = vcat(v, c)

Base.summary(c::ComposedFunction{<:Any,<:Features}) = summary.(unfoldchain(c))
