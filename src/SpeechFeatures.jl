# SPDX-License-Identifier: CECILL-C

module SpeechFeatures

using FFTW
using AudioSources

export FeaturesProperties,
       AddDeltas,
       Frames,
       FBANK,
       MFCC,
       STFT,
       Autocorr,

       HammingWindow,
       HannWindow,
       PoveyWindow,
       RectangularWindow,

       load

include("utils.jl")
include("features.jl")

export SpecAugment

#include("specaugment.jl")
# Feature extractors
#export FbankExtractor, MfccExtractor, StftExtractor
#include("extractors.jl")

end
