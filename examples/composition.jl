### A Pluto.jl notebook ###
# v0.19.42

using Markdown
using InteractiveUtils

# ╔═╡ 17d97d32-27c9-11ef-21c2-6bb54be94cb9
begin
	using Pkg
	Pkg.activate("../")
	using Revise
	using SpeechFeatures
	using Plots
end

# ╔═╡ 8ffaa50a-26f0-4557-ae4b-e09c77252491
example = URLAudioSource("https://catalog.ldc.upenn.edu/desc/addenda/LDC93S1.wav")

# ╔═╡ 725b7078-2b56-4d13-beee-6cf08d28186f
#data, props=load(exemple)

# ╔═╡ 2f7f7cc8-d4d8-4977-89ac-f041f6c24bb3
#X, props = load(32000) |> Frames() |> STFT() |> FBANK(numfilters=15)  

# ╔═╡ 80df7db3-d1b3-4687-864d-002a2f1b3e0d
fbank=FBANK()

# ╔═╡ db38cd81-c3a8-4c59-9a85-163c1d96a050
#features = MFCC() ∘ fbank ∘ STFT() ∘ Frames()
features = Autocorr() ∘ Frames()

# ╔═╡ 94a78e57-65b6-4686-b358-0e15afd421ab
summary(fbank)

# ╔═╡ 3d1fd11a-8a02-4ba9-a35e-6fcb9423af89
X, props = features(load(example))

# ╔═╡ 5617ae7e-dc3c-4497-951e-19c3bf1d9d15
summary(props)

# ╔═╡ 9da33e5e-bfb7-4054-be00-9973c4630ff2
summary(features)

# ╔═╡ 05f22d42-f2ac-4702-8028-92dc8f2bd49a
heatmap(log.(abs.(X)))

# ╔═╡ a1e2033a-5a22-4a73-a134-1a2370174fae
# begin
# 	p=plot()
# 	plot!(p, eachrow(fbank.filters))
# 	#plot!(p, fbank.filters[2,:])
# end

# ╔═╡ Cell order:
# ╠═17d97d32-27c9-11ef-21c2-6bb54be94cb9
# ╠═8ffaa50a-26f0-4557-ae4b-e09c77252491
# ╠═725b7078-2b56-4d13-beee-6cf08d28186f
# ╠═2f7f7cc8-d4d8-4977-89ac-f041f6c24bb3
# ╠═80df7db3-d1b3-4687-864d-002a2f1b3e0d
# ╠═db38cd81-c3a8-4c59-9a85-163c1d96a050
# ╠═94a78e57-65b6-4686-b358-0e15afd421ab
# ╠═3d1fd11a-8a02-4ba9-a35e-6fcb9423af89
# ╠═5617ae7e-dc3c-4497-951e-19c3bf1d9d15
# ╠═9da33e5e-bfb7-4054-be00-9973c4630ff2
# ╠═05f22d42-f2ac-4702-8028-92dc8f2bd49a
# ╠═a1e2033a-5a22-4a73-a134-1a2370174fae
